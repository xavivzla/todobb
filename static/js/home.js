var sliderPrincipal = tns({
  container : '.banner-home-list',
  mouseDrag : true,
  items     : 1,
  nav       : true,
  controls  : false,
  lazyload  : true,
  speed     : 400,
  swipeAngle: false,
  onInit    : function() {
  }
})


// marcas
var sliderbrand = tns({
  container : '.slider-brand-list',
  mouseDrag : true,
  items     : 2,
  nav       : false,
  controls  : false,
  lazyload  : true,
  speed     : 400,
  swipeAngle: false,
  autoplay: true,
  autoplayHoverPause: true,
  autoplayButtonOutput: false,
  autoplayTimeout: 2000,
  onInit    : function() {
  },
  responsive: {
    380: {
      items: 3
    },
    540: {
      items: 4
    },
    780: {
      items: 5
    },
    940: {
      items: 6
    }
  }
})



 /*
     * Replace all SVG images with inline SVG
     */
    jQuery('img.svg').each(function(){
      var $img = jQuery(this);
      var imgID = $img.attr('id');
      var imgClass = $img.attr('class');
      var imgURL = $img.attr('src');

      jQuery.get(imgURL, function(data) {
          var $svg = jQuery(data).find('svg');

          if(typeof imgID !== 'undefined') {
              $svg = $svg.attr('id', imgID);
          }
          if(typeof imgClass !== 'undefined') {
              $svg = $svg.attr('class', imgClass+' replaced-svg');
          }

          $svg = $svg.removeAttr('xmlns:a');

          $img.replaceWith($svg);

      }, 'xml');

  });



  $('.category-tab-links-item').click(function(e){
    if($(e.target).hasClass('active')){
      return true
    }
    $('.category-tab-links-item').removeClass('active')
    $(e.target).addClass('active')
    let id = $(e.target).data('category')
    $('.category-tab-content').removeClass('active');
    $('.category-tab-content').css({
      'opacity': 0
    });
    $(`.category-tab-content[data-cont="${id}"`).addClass('active');
    $('.category-tab-content.active').stop().animate({
      opacity: 1,
    }, 500, function() {
      // Animation complete.
    });
  })
  $('.category-tab-links-item:first').trigger('click')




  var sliderDiscount = tns({
    container : '.slider-discount-list',
    mouseDrag : true,
    items     : 1,
    nav       : false,
    controls  : false,
    lazyload  : true,
    speed     : 400,
    swipeAngle: false,
    fixedWidth: 246,
    autoplayButtonOutput: false,
    gutter: 30,
    autoplay: true,
    autoplayHoverPause: true,
    autoplayTimeout: 3500,
    onInit    : function() {
    },
    responsive: {
      // 380: {
      //   items: 3
      // },
      // 540: {
      //   items: 4
      // },
      // 780: {
      //   items: 5
      // },
      1024: {
        items: 3,
        fixedWidth: 0
      }
    }
  })
  

$('.slider-discount-arrow-left').click(function(){
  sliderDiscount.goTo('prev');
})

$('.slider-discount-arrow-right').click(function(){
  sliderDiscount.goTo('next');
})
