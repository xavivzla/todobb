var toggleIcon = function(el, rm, ad) {
  if(el.classList.contains(`${rm}`)) {
    el.classList.remove(`${rm}`)
    el.classList.add(`${ad}`)
  } else {
    el.classList.remove(`${ad}`)
    el.classList.add(`${rm}`)
  }
}

// header
$('.header-mobile-menu').click(function(){
  $('.header-menu').addClass('active')
  $('.overlay').addClass('active')
})

$('.header-menu-header-right').click(function(){
  $('.overlay').removeClass('active')
  $('.header-menu').removeClass('active')
})

// search
$('.open-search').click(function(){
  $('.header-search-box').addClass('active')
  $('.overlay').addClass('active')
})

$('.overlay').click(function(){
  $('.overlay').removeClass('active')
  $('.header-search-box').removeClass('active')
  $('.header-menu').removeClass('active')
})



// FOOTER TOGGLE
var linkFooter = document.querySelectorAll('.footer__link')
arrLinkFooter = Array.from(linkFooter)

if(window.innerWidth < 1024)
  arrLinkFooter.map(el => {
    el.addEventListener('click', function() {
      if(el.nextElementSibling != null && el.nextElementSibling.nodeName == 'UL') {
        var iconLink = el.querySelector('.footer__icon-down')
        toggleIcon(iconLink, 'icon-flecha-abajo', 'icon-flecha-arriba')
        $(this)
          .next()
          .slideToggle('fast')
      } else {
        return false
      }
    })
  })

  $('.overlay').on('click', function(){
    $('.card-sidebar').removeClass('active');
    $('body').removeClass('active')
  })

  $('.header-top-middle-search input').click(function(){
    $('.header-search-box').addClass('active')
  })


  $('.header-mobile-btn').click(function(){
    $('.header-mobile-nav-ctn').addClass('active')
    $('.overlay').addClass('active')

  })
  $('.header-mobile-nav-close').click(function(){
    $('.header-mobile-nav-ctn').removeClass('active')
    $('.overlay').removeClass('active')
  })
  $('.overlay').click(function(){
    $('.header-mobile-nav-ctn').removeClass('active')
  })
  $('.header-mobile-nav-list-link').click(function(){
    $(this).toggleClass('active')
  })


  $('.header-menu-top-link.cart').click(function(){
    $('.card-sidebar').addClass('active')
    $('.overlay').addClass('active')

  })

  $('.header-mobile-item.cart').click(function(){
    $('.card-sidebar').addClass('active')
    $('.overlay').addClass('active')

  })

  